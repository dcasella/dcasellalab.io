---
date: "2016-11-29T14:08:45+01:00"
title: "K-Means"
categories:
  - University
  - Pinned
tags:
  - C
  - Common Lisp
  - Prolog
  - Ruby
---

> 2016 - 2016  
> [GitHub Repository](https://github.com/dcasella/k-means)

University course: Programming languages.

Implementation of Lloyd's k-means algorithm in multiple languages.
The course taught functional and logical programming paradigms, and the course's final project required a Common Lisp and Prolog implementation of the algorithm.
I worked on the C and Ruby versions to get a better understanding of those languages.
