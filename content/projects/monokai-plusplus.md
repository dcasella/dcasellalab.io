---
date: "2017-11-29T14:15:01+01:00"
title: "Monokai++"
linkTitle: "monokai-plusplus"
categories:
  - Personal
---

> 2017 - Present  
> [GitHub Repository](https://github.com/dcasella/monokai-plusplus)

Revisitation of the Monokai color scheme, put together in a theme for Sublime Text 3 and Visual Studio Code.
