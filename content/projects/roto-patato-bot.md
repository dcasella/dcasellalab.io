---
date: "2017-11-29T14:11:59+01:00"
title: "Roto Patato Bot"
categories:
  - Personal
tags:
  - Python
---

> 2017 - 2017  
> [GitLab Repository](https://gitlab.com/dcasella/roto-patato-bot)

Simple Telegram bot with commands for retrieving movie ratings from various review websites.
The name is a reference to an inside joke between friends... sorry.
