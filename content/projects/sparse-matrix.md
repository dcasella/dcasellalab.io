---
date: "2017-11-29T14:10:37+01:00"
title: "Sparse Matrix"
categories:
  - University
tags:
  - C++
---

> 2017 - 2017  
> [GitHub Repository](https://github.com/dcasella/sparse-matrix)

University course: Programming and system administration.

Implementation of a templated 2D matrix class where zero elements are more frequent than non-zero elements.
This type of class minimizes the amount of stored data while losing the constant-time random access to matrix elements.
