---
title: "Home"
linkTitle: "/ var / www"
---

Hi, I'm Davide (if it wasn't clear enough). I'm an undergraduate in Computer Science with a passion for gameplay programming and script automation, always interested in learning advanced concepts and understanding the intricacies of programming languages.

For work experience and education, please refer to my [LinkedIn](https://www.linkedin.com/in/d-casella/) profile.
